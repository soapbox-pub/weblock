# Weblock

Functions to lock native web APIs after they have been used.

## Usage

The basic idea is that you access a resource as soon as possible when the page loads, and then lock it to prevent further access.

While it should be used alongside a secure [Content Security Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) and other security measures, this library offers additional client-side hardening against XSS attacks.

```ts
const auth = localStorage.getItem('auth'); // returns data

Weblock.storages.lockKey('auth'); // Lock the storage key

localStorage.getItem('auth'); // throws an Error
```

It's suggested to load the data into a class instance that only exposes functions to perform the actions you need without exposing the data itself.

For example:

```ts
class APIClient {
  #accessToken: string | null;

  constructor() {
    this.#auth = localStorage.getItem('accessToken');
    Weblock.storages.lockKey('accessToken');
  }

  async get(path: string): Promise<Response> {
    const headers: HeadersInit = {};

    if (this.#accessToken) {
      headers['Authorization'] = `Bearer ${this.#accessToken}`;
    }

    return fetch(path, { headers });
  }

  // ...other functions
}

// Initialize the API client as early as possible in your application
export const api = new APIClient();
```

## Installation

TODO: npm package

## License

This is free and unencumbered software released into the public domain. By submitting patches to this project, you agree to dedicate any and all copyright interest in this software to the public domain.
