/** Functions to lock the Worker API. */
export class WorkersLock {
  /** Lock the Worker API from creating new workers. */
  static lock(): void {
    Worker.constructor = function () {
      throw new Error('Worker is locked');
    };
  }
}
