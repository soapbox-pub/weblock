import { assertEquals, assertThrows } from 'jsr:@std/assert';

import { StoragesLock } from './StoragesLock.ts';

Deno.test('StoragesLock.lockKey', () => {
  localStorage.setItem('token', 'super secret!');

  // sanity check
  assertEquals(localStorage.getItem('token'), 'super secret!');

  // lock the key
  StoragesLock.lockKey('token');

  // check that the key is locked
  assertThrows(() => {
    localStorage.getItem('token');
  });

  // access via prototype
  assertThrows(() => {
    Object.getPrototypeOf(localStorage).getItem.bind(localStorage)('token');
  });

  // direct property access
  assertThrows(() => {
    localStorage['token'];
  });
});
