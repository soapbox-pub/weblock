import { ServiceWorkerLock } from './ServiceWorkerLock.ts';
import { StoragesLock } from './StoragesLock.ts';
import { WorkersLock } from './WorkersLock.ts';

/** Utilities to lock Web APIs from being accessed after the application has obtained the necessary data. */
export class WebLock {
  /** Functions for locking the Storage API. */
  static readonly storages = StoragesLock;

  /** Functions to lock the ServiceWorker API. */
  static readonly serviceWorker = ServiceWorkerLock;

  /** Functions to lock the Worker API. */
  static readonly workers = WorkersLock;
}
