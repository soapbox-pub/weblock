/** Functions to lock the ServiceWorker API. */
export class ServiceWorkerLock {
  /** Lock the Service Worker API from registering scripts. */
  static lock(): void {
    if ('serviceWorker' in navigator) {
      const proto = Object.getPrototypeOf(navigator.serviceWorker);
      proto.register = function () {
        throw new Error('ServiceWorker is locked');
      };
    }
  }
}
