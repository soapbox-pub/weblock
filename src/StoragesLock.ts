/** Functions for locking the Storage API. */
export class StoragesLock {
  static #lockedKeys: Set<string> = new Set();
  static #proto: Storage = Object.getPrototypeOf(localStorage ?? sessionStorage);

  static {
    const _getItem = this.#proto.getItem;

    // Redefine `getItem` to throw an error if the key is locked.
    this.#proto.getItem = function (key: string) {
      if (StoragesLock.#lockedKeys.has(key)) {
        throw new Error(`${key} is locked`);
      } else {
        return _getItem.bind(this)(key);
      }
    };
  }

  /** Lock a key from being accessed by `localStorage` and `sessionStorage`. */
  static lockKey(key: string): void {
    StoragesLock.#lockedKeys.add(key);

    // Prevent direct property access of key.
    Object.defineProperty(this.#proto, key, {
      get() {
        throw new Error(`${key} is locked`);
      },
    });
  }
}
